import cv2
import os
import numpy as np
import collections


def read_video(vid):
    vidcap = cv2.VideoCapture(vid)
    success,image = vidcap.read()
    res = []
    while success:
        image = cv2.resize(image, (50, 50))
        res.append(image.ravel())
        success, image = vidcap.read()

    return res


def read_video_from_dir(dir):
    data = []

    for v in os.listdir(dir):
        data += read_video(dir + '/' + v)
    data = np.matrix(data)

    return data


def get_files_per_dirs(root, dirs):
    files_for_class = collections.defaultdict(list)

    for class_dir in dirs:
        for f in os.listdir(root + '/' + class_dir):
            full = root + '/' + class_dir + '/' + f

            # чтобы случайно не передали локальную директорию
            path = os.path.normpath(full)
            tokens = path.split(os.sep)

            if len(tokens) >= 2:
                files_for_class[class_dir].append(full)

    return files_for_class


class VideoLoader:
    def __init__(self, video_path):
        self._path = video_path
        self._cap = cv2.VideoCapture(video_path)
        self._total_frames = int(self._cap.get(cv2.CAP_PROP_FRAME_COUNT))
        self._fps = int(self._cap.get(cv2.CAP_PROP_FPS))

        if self._fps == 0:
            raise Exception('kek')

        self._time_length = self._total_frames / self._fps

        self._last_index = 0
        self._last_images = []

    def __getitem__(self, i):
        if i >= 0 and i <= self._total_frames:
            self._cap.set(cv2.CAP_PROP_POS_MSEC , (i * 1000 / self._fps))

            if i >= self._last_index and i < self._last_index + len(self._last_images):
                return self._last_images[i - self._last_index]
            else:
                self._last_images = []
                self._last_index = i
                for i in range(min(4, self._total_frames - i)):
                    ret, frame = self._cap.read()
                    if ret:
                        self._last_images.append(frame)
                return self._last_images[0]
        return None
    
    def __len__(self):
        return self._total_frames
    
    def moment(self, i):
        return self._path, i / self._total_frames 


class VideoDirLoader:
    def __init__(self, video_dir):
        self._loaders = []
        self._total_frames = 0

        for v in os.listdir(video_dir):
            video_path = video_dir +  '/' + v
            try:
                loader = VideoLoader(video_path)
                self._loaders.append(loader)
                self._total_frames += len(loader)
            except:
                print('не удалось открыть:', video_path)
    
    def __getitem__(self, i):
        cnt = 0
        for l in self._loaders:
            if i < cnt + len(l):
                return l[i-cnt]
            cnt += len(l)
        return None

    def __len__(self):
        return self._total_frames

    def moment(self, i):
        cnt = 0
        for l in self._loaders:
            if i < cnt + len(l):
                return l.moment(i-cnt)
            cnt += len(l)
        return None

class VideoProccessor:
    def __init__(self, video, func, history=0, interval=5):
        if type(video) != list:
            self._videos = [video]
        else:
            self._videos = video
        self._func = func
        self._history = history
        self._interval = interval
    
    def __getitem__(self, i):
        frames = []
        if self._history == 0:
            frames = [vid[i] for vid in self._videos]
        else:
            if i >= (self._history * self._interval - 1):
                for vid in self._videos:
                    frames.append([])
                    for j in range(self._history):
                        frames[-1].append(vid[i - j * self._interval])
                    frames[-1].reverse()

        if len(frames) > 0:
            return self._func(*frames)

        return None

    def moment(self, i):
        return self._videos[0].moment(i)
    
    def __len__(self):
        return len(self._videos[0])
