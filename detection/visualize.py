import cv2
import numpy as np
import PySimpleGUI as sg

WINDOW_WIDTH = 1200
WINDOW_HEIGHT = 900
FPS = 15
VIDEOS_IN_ROW=2

def make_window(videos, videos_in_row, num_frames, wwidth, wheight, iheight):
    layout_videos = []

    layout = [
        #[sg.Text('Fish processing', size=(15, 1), font='Helvetica 20')],
    ]

    image_layout_index = 0
    while image_layout_index < len(videos):
        if image_layout_index % videos_in_row == 0:
            #for i in range(min(videos_in_row, len(videos) - image_layout_index)):
            #    layout_videos.append([[sg.Text('    ')], [sg.Text(videos[image_layout_index + i][1], size=(14, 1))]])
            layout_videos.append([])

        img = [[sg.Image(key=f'-IMAGE{image_layout_index}-')]]
        layout_videos[-1] += [sg.Column(img, size=(wwidth / 2 - 25, iheight)), sg.VSeparator()]
        image_layout_index += 1
    
    layout += [
        [sg.Column(layout_videos, size=(wwidth, wheight), scrollable=True, vertical_scroll_only=True)],
        [sg.Slider(range=(0, num_frames), size=(60, 10), orientation='h', key='-SLIDER-'), sg.Button('Exit', font='Helvetica 14')],
    ]

    sg.theme('Black')
    window = sg.Window('Fisher', layout, no_titlebar=False, location=(0, 0))
    return window


def show_video_gallary(videos, wwidth=WINDOW_WIDTH, wheight=WINDOW_HEIGHT, fps=FPS, videos_in_row=VIDEOS_IN_ROW):
    num_frames = 0
    for v in videos:
        num_frames += len(v[0])

    frame = videos[0][0][0]
    h, w = frame.shape[0], frame.shape[1]
    new_w = int(wwidth/ videos_in_row + 50 * videos_in_row)
    new_h = int(new_w * (h / w))

    window = make_window(videos, videos_in_row, num_frames, wwidth + 100 * videos_in_row, wheight, new_h)
    slider_elem = window['-SLIDER-']

    timeout = 1000 // fps
    
    while True:
        event, values = window.read(timeout=timeout)
        if event in ('Exit', None):
            break

        cur_frame = 0
        if int(values['-SLIDER-']) != cur_frame-1:
            cur_frame = int(values['-SLIDER-'])
        
        for i, vid in enumerate(videos):
            frame = vid[0][cur_frame]
            if frame is None:
                continue
            if len(frame.shape) == 2 or frame.shape[2] == 1:
                frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)

            frame = cv2.resize(frame, (new_w, new_h))
            imgbytes = cv2.imencode('.ppm', frame)[1].tobytes()

            image_elem = window[f'-IMAGE{i}-']
            image_elem.update(data=imgbytes)

        if cur_frame + 1 < num_frames:
            cur_frame += 1

        slider_elem.update(cur_frame) 