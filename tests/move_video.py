import context as _
import detection.utils as utils
import matplotlib.pylab as plt
import datetime
import os
import re

def listvideos(dir):
    for file in os.listdir(dir):
        full = dir + '/' + file
        if os.path.isdir(full):
            for v in listvideos(full):
                yield v
        else:
            yield dir, file


beg = datetime.datetime(2023, 2, 10, 10, 30, 0)
end = datetime.datetime(2023, 2, 10, 16, 0, 0)

path = '/mnt/c/Users/binarycat/YandexDisk/FishMl'
dst = '/mnt/c/Users/binarycat/YandexDisk/FishMl/video_increasingO2'

for dir, file in listvideos(path):
    full = dir + '/' + file

    m1 = re.search('IPCAM_\d+-\d+-(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2}).mp4', file)
    if m1 is not None:
        dtime = datetime.datetime(
            int(m1.group(1)), int(m1.group(2)),
            int(m1.group(3)), int(m1.group(4)),
            int(m1.group(5)), int(m1.group(6)))

        stime = utils.stime(dtime)
        if dtime >= beg and dtime <= end:
            print('MOVING', file, f'{dst}/IPCAM_{stime}.mp4')
            os.rename(full, f'{dst}/IPCAM_{stime}.mp4')
        else:
            print('RENAMING', file, f'{dir}/IPCAM_{stime}.mp4')
            os.rename(full, f'{dir}/IPCAM_{stime}.mp4')

