import context as _
import cv2
import numpy as np
import detection.filter as filter
import detection.load as load
import detection.preprocess as prep
import detection.visualize as visualize
import detection.utils as utils
import detection.transform as transform
from detection.load import VideoProccessor
import matplotlib.pyplot as plt

source_video = load.VideoDirLoader('/mnt/c/Users/binarycat/YandexDisk/FishMl/watch/1')
prep_video = VideoProccessor(source_video, prep.prepare_frame)
gray_video = VideoProccessor(prep_video, lambda im: cv2.cvtColor(im, cv2.COLOR_BGR2GRAY))

mean, std = utils.mean_std_metrics(gray_video)
norm_video = VideoProccessor(gray_video, lambda im: utils.normalize((im - mean) / std))
eq_video = VideoProccessor(norm_video,
    lambda im: cv2.equalizeHist(im.astype(np.uint8)))
eq_video_color = VideoProccessor(eq_video, lambda im: cv2.cvtColor(im, cv2.COLOR_GRAY2BGR))

smoothed_gaussian = VideoProccessor(eq_video,
    lambda im: cv2.GaussianBlur(im, (11, 11), 2))

mean_image = utils.mean_image(smoothed_gaussian)
dif_video = VideoProccessor(smoothed_gaussian, 
    lambda im: transform.image_dif(im, mean_image))

thres = VideoProccessor(dif_video, 
    lambda im: cv2.adaptiveThreshold(im, 255,
        cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 151, -10))
morph = VideoProccessor(thres, transform.fill_gaps)
heatmap = VideoProccessor(morph, transform.heatmap_filter, 3)
final = VideoProccessor([eq_video_color, heatmap], transform.filter_contours)

optical_flow = VideoProccessor(norm_video, transform.optical_flow, 2)
color_flow = VideoProccessor(optical_flow, transform.color_flow)
masked_flow = VideoProccessor([color_flow, final], utils.apply_mask)

blended = VideoProccessor([eq_video_color, masked_flow], transform.draw_blend)
contoured = VideoProccessor([blended, final], transform.draw_contours)

visualize.show_video_gallary([
    (source_video, 'source'),
    (eq_video, 'eq'),
    (dif_video, 'dif'),
    (final, 'final'),
    (contoured, 'contoured')
])