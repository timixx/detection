import context as _
import cv2
import numpy as np
import detection.transform as transform
import detection.filter as filter
import detection.load as load
import detection.preprocess as prep
import detection.visualize as visualize
import detection.utils as utils
from detection.load import VideoProccessor


source_video = load.VideoDirLoader('data/videos')
prep_video = VideoProccessor(source_video, prep.prepare_frame)
gray_video = VideoProccessor(prep_video, lambda im: cv2.cvtColor(im, cv2.COLOR_BGR2GRAY))

test_image = cv2.equalizeHist(gray_video[0])
mean_image = cv2.equalizeHist(utils.mean_image(gray_video))
heat_map = sb.heatmap(mean_image)
dif = transform.image_dif(test_image, mean_image)

cv2.imshow('im', dif)
cv2.waitKey(0)
