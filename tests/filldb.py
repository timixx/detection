import context as _
import detection.dbtools as dbtools


dbtools.connect(dbtools.get_labels)
dbtools.connect(dbtools.insert_videos)
dbtools.connect(dbtools.assign_labels)