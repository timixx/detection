import context as _
import cv2
import numpy as np
import detection.filter as filter
import detection.load as load
import detection.preprocess as prep
import detection.visualize as visualize
import detection.utils as utils
import detection.transform as transform
from detection.load import VideoProccessor
import matplotlib.pyplot as plt

source_video = load.VideoDirLoader('data/videos/night')
prep_video = VideoProccessor(source_video, prep.prepare_frame)
gray_video = VideoProccessor(prep_video, lambda im: cv2.cvtColor(im, cv2.COLOR_BGR2GRAY))

mean, std = utils.mean_std_metrics(gray_video)
norm_video = VideoProccessor(gray_video, lambda im: utils.normalize((im - mean) / std))
eq_video = VideoProccessor(norm_video,
    lambda im: cv2.equalizeHist(im.astype(np.uint8)))
eq_video_color = VideoProccessor(eq_video, lambda im: cv2.cvtColor(im, cv2.COLOR_GRAY2BGR))

smoothed_gaussian = VideoProccessor(eq_video,
    lambda im: cv2.GaussianBlur(im, (11, 11), 2))

mean_image = utils.mean_image(smoothed_gaussian)
dif_video = VideoProccessor(smoothed_gaussian, 
    lambda im: transform.image_dif(im, mean_image))

thres = VideoProccessor(dif_video, 
    lambda im: cv2.adaptiveThreshold(im, 255,
        cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 151, -10))
morph = VideoProccessor(thres, transform.fill_gaps)
heatmap = VideoProccessor(morph, transform.heatmap_filter, 3)
final = VideoProccessor([eq_video_color, heatmap], lambda g, m: transform.filter_contours(g, m))

optical_flow = VideoProccessor(norm_video, transform.optical_flow, 2)
masked_flow = VideoProccessor([optical_flow, final], utils.apply_mask)

last = 0
cnt = 0

for i in range(20, len(optical_flow)):
    frame = masked_flow[i]
    if frame is None: 
        continue

    frame = frame[..., 1]
    
    score = frame.sum() / 100000
    m = optical_flow.moment(i)
    t = m[1] * 60

    #print('VALUE', m[0], last, score)

    if t - last > 1:
        if cnt >= 1:
            print('ИСПУГ', m[0], last, score)
        cnt = 0


    if t > 1.0 and score >= 0.02 and score <= 0.45:
        #print('ПОПУГ', m[0], t, score, 't', t, 'last', last)
        if t - last <= 1:
            cnt = cnt + 1
        last = t