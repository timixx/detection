import serial
import struct
import datetime
import detection.utils as utils

PORT = '/dev/ttyUSB0'
TIMEOUT = 60
FOLDER = '/home/pi/data/'

def convert(v):
    return float(hex(v)[2:])

def main(serialPort, f_output, start_time):
    while True:
        current_time = datetime.datetime.now()
        if (current_time - start_time).seconds > TIMEOUT:
            return

        if serialPort.in_waiting == 0:
            print('continue')
            continue

        b = serialPort.read_until(b'\xff')

        if len(b) == 11:
            print(
                b, b[1], b[2], b[3], b[4], b[5], b[6],
                struct.unpack('h', b[1:3])[
                    0], struct.unpack('h', b[2:4])[0],
                struct.unpack('h', b[3:5])[
                    0], struct.unpack('h', b[4:6])[0],
                struct.unpack('h', b[5:7])[0]
            )

            Val = convert(b[1])
            Val += convert(b[2]) / 100.0

            Tf = convert(b[4] & 0b00001111)
            Tn = convert(b[4] & 0b11110000)
            Td = convert(b[3])
            Temp = Td * 10.0 + Tn / 10.0 + Tf / 10.0

            mode = b[5]
            mode_str = 'None'
            if mode == 20:
                mode_str = 'O2'
                Val *= 10.0

            elif mode == 18:
                mode_str = 'DO'

            elif mode == 13:
                mode_str = 'PH'

            elif mode == 49:
                mode_str = 'ORP'

            elif mode == 1:
                mode_str = 'Cond'

            elif mode == 5:
                mode_str = 'TDS'

            elif mode == 9:
                mode_str = "Salt"

            elif mode == 2:
                mode_str = 'Wait'

            print(f'T: {Temp}, {mode_str}: {Val}' + '{0:08b}'.format(b[4]))
            time = utils.stime(datetime.datetime.now())
            print(f'{time}, {Temp}, {mode_str}, {Val}', file=f_output)

        else:
            print('UNRECOGNIZED:', b)

    
start_time = datetime.datetime.now()
with open(FOLDER + utils.stime(start_time) + '.txt', 'w') as f_output:
    current_time = datetime.datetime.now()
    while True:
        current_time = datetime.datetime.now()
        if (current_time - start_time).seconds >= TIMEOUT:
            break

        try:
            with serial.Serial(
                port=PORT,
                baudrate=9600,
                bytesize=8,
                timeout=3,
                stopbits=serial.STOPBITS_ONE) as serialPort:
                    main(serialPort, f_output, start_time)
        except:
            print ('reconnect')